# BungaLAN 2019 Steamcache Setup

### Setup

Proxmox (`https://10.42.0.10:8006`) running two VM's:
- One VM that runs pfSense. (`http://10.42.0.1/`)
- One VM that runs Ubuntu 18.04.1 LTS. This VM runs Docker which in turn runs our game caches.

### Docker Instances

We are using the following IP addresses:
 - STEAMCACHE_IP `10.42.0.31`
 - BLIZZARDCACHE_IP	`10.42.0.32`
 - FRONTIERCACHE_IP	`10.42.0.33`
 - ORIGINCACHE_IP `10.42.0.34`
 - UPLAYCACHE_IP `10.42.0.35`
 - WSUSCACHE_IP `10.42.0.36`
 - RIOTCACHE_IP	`10.42.0.37`
 
To run the containers:
 
#### Steam
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-steam \
 -v /steamcache/steam/data:/data/cache \
 -v /steamcache/steam/logs:/data/logs \
 -p 10.42.0.31:80:80 \
 steamcache/generic:latest
 ```
   
#### Blizzard

```bash
docker run -d \
 --restart unless-stopped \
 --name cache-blizzard \
 -v /steamcache/blizzard/data:/data/cache \
 -v /steamcache/blizzard/logs:/data/logs \
 -p 10.42.0.32:80:80 \
 steamcache/generic:latest
```
   
#### Frontier
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-frontier \
 -v /steamcache/frontier/data:/data/cache \
 -v /steamcache/frontier/logs:/data/logs \
 -p 10.42.0.33:80:80 \
 steamcache/generic:latest
 ```
 
#### Origin
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-origin \
 -v /steamcache/origin/data:/data/cache \
 -v /steamcache/origin/logs:/data/logs \
 -p 10.42.0.34:80:80 \
 steamcache/generic:latest
 ```
 
#### Uplay
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-uplay \
 -v /steamcache/uplay/data:/data/cache \
 -v /steamcache/uplay/logs:/data/logs \
 -p 10.42.0.35:80:80 \
 steamcache/generic:latest
 ```
 
#### Windows Updates
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-windows \
 -v /steamcache/windows/data:/data/cache \
 -v /steamcache/windows/logs:/data/logs \
 -p 10.42.0.36:80:80 \
 steamcache/generic:latest
 ```
 
#### Riot
```bash
docker run -d \
 --restart unless-stopped \
 --name cache-riot \
 -v /steamcache/riot/data:/data/cache \
 -v /steamcache/riot/logs:/data/logs \
 -p 10.42.0.37:80:80 \
 steamcache/generic:latest
 ``` 
 
#### DNS
```bash
docker run \
 --restart unless-stopped -d \
 --name steamcache-dns \
 -p 10.42.0.20:53:53/udp \
 -e UPSTREAM_DNS=1.1.1.1 \
 -e STEAMCACHE_IP=10.42.0.31 \
 -e BLIZZARDCACHE_IP=10.42.0.32 \
 -e FRONTIERCACHE_IP=10.42.0.33 \
 -e ORIGINCACHE_IP=10.42.0.34 \
 -e UPLAYCACHE_IP=10.42.0.35 \
 -e WSUSCACHE_IP=10.42.0.36 \
 -e RIOTCACHE_IP=10.42.0.37 \
 steamcache/steamcache-dns:latest
 ```
 
#### SNI
```bash
docker run \
 --restart unless-stopped -d \
 --name sniproxy \
 -p 443:443 \
 steamcache/sniproxy:latest
 ```
 
### Miscellaneous Commands

#### Getting in the steamcache-warmer
`docker run -it --dns 10.42.0.20 --name=steamcmd y0sh1/steamcache-warmer bash`

#### Tail log files on a cache container
`sudo docker exec -it cache-steam tail -f /data/logs/access.log`

#### Check cache size
`sudo du -sh -- /steamcache/*`

#### Start all running Docker containers
`sudo docker start $(docker ps -aq)`

#### Stop all running Docker containers
`sudo docker stop $(docker ps -aq)`

#### Add all IP addresses to the ens18 interface
`base=10.42.0; for ip in $(seq 31 37); do full_ip=$base.$ip; echo "Configuring "$full_ip; sudo ip address add $full_ip dev ens18 done`

#### Chek what IP addresses are assigned to an interface
`ip addr show ens18`

### Contributing
We zien je pull request graag tegemoet.

### Resources
 - https://github.com/steamcache/generic
 - https://github.com/steamcache/sniproxy
 - https://github.com/steamcache/steamcache-dns
 - https://linustechtips.com/main/topic/962655-steam-caching-tutorial

